provider "aws" {
  region = "eu-west-3"
}

variable vpc_cidr_block {}
variable subnet_cidr_block {}
variable avail_zone {}
variable env_prefix {}
variable my_ip {}
variable instance_type {}

resource "aws_vpc" "myapp-vpc" {
    cidr_block = var.vpc_cidr_block
    tags = {
              Name: "${var.env_prefix}-vpc"
            }
}

resource "aws_subnet" "myapp-subnet-1" {
    vpc_id = aws_vpc.myapp-vpc.id
    cidr_block = var.subnet_cidr_block
    availability_zone = var.avail_zone
    tags = {
              Name: "${var.env_prefix}-subnet-1"
            }    
}

resource "aws_internet_gateway" "myapp-igw" {
  vpc_id = aws_vpc.myapp-vpc.id
  tags = {
          Name: "${var.env_prefix}-igw"
        }
}
 
resource "aws_default_route_table" "main-rtb" {
  default_route_table_id =  aws_vpc.myapp-vpc.default_route_table_id
  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.myapp-igw.id
  }
  tags = {
  Name = "${var.env_prefix}-main-rtb"
  }
}

#when not wanting to define new sg use this and add name definition: resource "aws_security_group" "myapp-sg" {
resource "aws_default_security_group" "default-sg" {
  #name = "myapp-sg"
  vpc_id = aws_vpc.myapp-vpc.id
  /*access ssh on port 22*/
  ingress {
    from_port = 22 
    to_port = 22
    protocol = "tcp"
    cidr_blocks = [var.my_ip]
  }
  /*access from browser only by port 8080*/
  ingress {
    from_port = 8080
    to_port = 8080
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  /*outgoing for example istallations, fetch docker image*/
  egress {
    from_port = 0
    to_port = 0
    protocol = "-1" #any protocol
    cidr_blocks = ["0.0.0.0/0"]
    prefix_list_ids = [] 
  }
  tags = {
  Name = "${var.env_prefix}-default-sg"
  }

}

data "aws_ami" "latest-amazon-linux-image" {
  most_recent = true
  owners = ["amazon"]
  filter {
    name = "name"
    values = ["amzn2-ami-hvm-*-x86_64-gp2"]
  }
  filter {
    name = "virtualization-type"
    values = ["hvm"]
  }
  
}

#check ami_id
output "aws_ami_id" {
  value = data.aws_ami.latest-amazon-linux-image.id
  }

resource "aws_instance" "myapp-server" {
  ami = data.aws_ami.latest-amazon-linux-image.id
  instance_type = var.instance_type
  #optional ids:
  subnet_id = aws_subnet.myapp-subnet-1.id
  vpc_security_group_ids = [aws_default_security_group.default-sg.id]
  availability_zone = var.avail_zone

  # we need public ip address to reach ssh etc.
  associate_public_ip_address = true
  #following server-key-pair was created manually in aws see notes (then downloaded)
  key_name = "server-key-pair"

  tags = {
    Name = "${var.env_prefix}-server"
  }
}


/* 
#route_table definition if aws_default_route_table is not in use:
resource "aws_route_table" "myapp-route-table" {
  vpc_id = aws_vpc.myapp-vpc.id
  route {
        cidr_block = "0.0.0.0/0"
        gateway_id = aws_internet_gateway.myapp-igw.id
        }
  tags = {
         Name: "${var.env_prefix}-rtb"
         }

} 

#association is also not needed then anymore, as only needed when not referenced to default
resource "aws_route_table_association" "a-rtb-subnet" {
  subnet_id = aws_subnet.myapp-subnet-1.id
  route_table_id = aws_route_table.myapp-route-table.id
} */

output "vpc-id" {
  value = aws_vpc.myapp-vpc.id
}

output "subnet-1-id" {
  value = aws_subnet.myapp-subnet-1.id
}